# Frequently Asked Questions

## Problems while compiling

### Ubuntu 18.04 LTS and CMake >= 3.14
To install cmake >= 3.14 in Ubuntu 18.04 use snap and use the snap version directly if
you have the apt version installed in parallel.
```
# sudo snap install cmake
# /snap/bin/cmake --version
cmake version 3.16.3
...
# /usr/bin/cmake --version
cmake version 3.10.2
```
To use the snap version of cmake in the documentation just replace the cmake command
with the full path to the cmake command in snap.
```
# cd myRepo
# /snap/bin/cmake -Bbuild -H. ...
```

### Ubuntu 18.04 LTS and googletest
Ubuntu does not provide the file libgtest_main.so in their libgtest-dev package 
which leads to an error during FindPackage in cmake. Either you install googletest 
and googlemock by hand or you disable the unit tests by adding ```-DBUILD_UNITTESTS=OFF``` 
to the cmake command like
```
# cmake -Bbuild -H. -DBUILD_UNITTESTS=OFF
```
But do not worry, all packages should still be fully functional.
