# larry.robotics

## Quick Start

For a quick start use the ```initializeRepos.sh``` script to clone all required
repositories for larry.
```
# ./initializeRepos.sh https
```
or use the following parameter to clone via ssh
```
# ./initializeRepos.sh ssh
```

## How everything is structured
The basic services to access the camera, ultra sonic sensor and to drive larry can be found
in the [larry-services](https://gitlab.com/el.chris/larry-services) repository. To connect
to larry and to drive remotely around with him you need to install [larry-ui](https://gitlab.com/el.chris/larry-ui).
My own build from scratch 3D engine, called [3Delch](https://gitlab.com/el.chris/3Delch) is which
holds everything together and is used by larry-ui and the larry-services.


## Documentation

A specific detailed documentation can be found in the ```README.md``` file in 
each repository.

[FAQ - When something goes wrong in the tutorials](/FAQ.md)

[Howto build your own Larry T2](/larry-T2-build-instructions.md)

[Use QEMU in a chroot environment to configure your Raspberry Pi SD Card](/use-qemu-to-chroot-arm-linux.md)
