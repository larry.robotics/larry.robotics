# Use QEMU in a chroot environment to configure your Raspberry Pi SD Card

## Install required packages

We are assuming that you are using ArchLinux, if not you have to adjust the following
commands and packages to your distribution specific ones.

To mount and chroot and SD Card with a Raspberry Pi image we require the following
packages.
 * qemu
 * qemu-arch-extra
 * qemu-arm-static

```
# pacman -S qemu qemu-arch-extra
```
The last one needs to be obtained from the AUR
```
# git clone https://aur.archlinux.org/qemu-arm-static
# cd qemu-arm-static
# makepkg -si
```

## Mount SD Card Image

Put your SD Card into your SD Card reader and look up under which device file it is registered.
```
# parted -l
...
Model: Mass Storage Device (scsi)
Disk /dev/sdc: 31.9GB
Sector size (logical/physical): 512B/512B
Partition Table: msdos
Disk Flags:

Number  Start   End     Size    Type     File system  Flags
 1      1049kB  106MB   105MB   primary  fat16        lba
 2      106MB   31.9GB  31.8GB  primary  ext4
...
```
In our case it is ```/dev/sdc``` so we type the following to mount the image.
```
# sudo mount /dev/sdc2 /mnt
# sudo mount /dev/sdc1 /mnt/boot
```
In the next step we need to mount all the system directories.
```
# sudo mount --bind /proc /mnt/proc
# sudo mount --bind /sys /mnt/sys
# sudo mount --bind /dev /mnt/dev
# sudo mount --bind /dev/pts /mnt/dev/pts
```

## Setup QEMU

To allow QEMU to work properly we need to register ``arm`` and ``aarch64`` emulation.
```
# su -
# echo ':arm:M::\x7fELF\x01\x01\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x02\x00\x28\x00:\xff\xff\xff\xff\xff\xff\xff\x00\xff\xff\xff\xff\xff\xff\xff\xff\xfe\xff\xff\xff:/qemu-wrapper:' > /proc/sys/fs/binfmt_misc/register
# echo ':aarch64:M::\x7fELF\x02\x01\x01\x00\x00\x00\x00\x00\x00\x00\x00\x00\x02\x00\xb7:\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xff\xfe\xff\xff:/usr/bin/qemu-aarch64:' > /proc/sys/fs/binfmt_misc/register
```
Before we can chroot into the SD Card we need to copy the ```qemu-arm-static```.
```
# sudo cp $(which qemu-arm-static) /mnt/usr/bin
```

## chroot

Now that everything is updated we can chroot into the environment.
```
# chroot /mnt/ /bin/bash
```
To verify that you are really inside an ARM emulated environment use ```uname``` which prints besides the kernel version also the architecture on which you are on.
```
# uname -a
Linux larry 5.4.8-arch1-1 #1 SMP PREEMPT Sat, 04 Jan 2020 23:46:18 +0000 armv7l GNU/Linux
```

Here you can setup, install, compile and run everything like you would be directly on a Raspberry Pi. When you finished your setup you can unmount everything and then put the SD Card into your Raspberry Pi.
```
# sudo umount /mnt/proc
# sudo umount /mnt/sys
# sudo umount /mnt/dev/pts
# sudo umount /mnt/dev
# sudo umount /mnt/boot
# sudo umount /mnt/
```
