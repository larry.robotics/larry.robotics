# Build your own Larry T2
If you encounter problems please take a look into the [FAQ](/FAQ.md).

## Purchase List

1. YahBoom smart tank robot kit: https://category.yahboom.net/collections/high-recommendation/products/g1tank. Do not buy the Raspberry Pi 3B+ version, either buy without Raspberry Pi or with Raspberry Pi 4!
2. (Optional) International Power Supply Adapter (YahBoom smart tank power supply uses the american standard)
3. (Optional) Raspberry Pi 4 with Micro SD Card (at least 16GB)
4. (Optional) Raspberry Pi 4 Power Supply
5. (Optional) SD Card with at least 16GB
6. (Optional) Micro HDMI Cable (to connect the Raspberry Pi 4 to a monitor)
7. (Optional) Intel RealSense Depth Camera D435: https://store.intelrealsense.com/buy-intel-realsense-depth-camera-d435.html
8. (Optional) Intel RealSense Tracking Camera T265: https://www.intelrealsense.com/tracking-camera-t265/

## Create the image for Larry

For Larry T2 we do not use the prebuilt image provided by YahBoom. We build our own image instead. For that 
we use ArchLinux since it is easy to handle, minimalistic and has
still everything we need.
In the following sections we describe how you configure your own Larry image with an active wlan access point and all necessary development tools installed.

To perform the necessary steps you should install the following tools:
 * wget
 * parted
 * tar

1. **Install basic image**: (Install instruction can also be found under: 
    https://archlinuxarm.org/platforms/armv8/broadcom/raspberry-pi-4#installation)

    1. Connect your SD Card to your computer and look up the device under which it is recognized. Normally, you can find it under ```Model: Mass Storage Device...```. Do not mix it up otherwise you destroy the data of your operating system!

    ```
    # parted -l
    Model: SAMSUNG MZVLB512HAJQ-00000 (nvme)
    Disk /dev/nvme0n1: 512GB
    Sector size (logical/physical): 512B/512B
    Partition Table: msdos
    Disk Flags:

    Number  Start   End    Size   Type     File system  Flags
     1      1049kB  512GB  512GB  primary  btrfs        boot

    Model: Mass Storage Device (scsi)
    Disk /dev/sdc: 31.9GB
    Sector size (logical/physical): 512B/512B
    Partition Table: msdos
    Disk Flags:

    Number  Start   End     Size    Type     File system  Flags
     1      1049kB  106MB   105MB   primary  fat16        lba
     2      106MB   31.9GB  31.8GB  primary
    ```

    2. **Important!** Do not use the wrong device in this step
     otherwise you destroy all the data of your system drive! In our
     example we found ```/dev/sdc``` as SD Card device. Start partitioning your SD Card with
    ```# fdisk /dev/sdX``` (Replace X with your sd device letter.). Enter the following characters and press return: 
    (*Hint: If fdisk asks you if you would like to remove the signature please answer it with "Yes".* )
        1. ```o``` - Clear partition table.
        2. ```n```, ```p```, ```1```, ```return```, ```+100M``` - Create boot partition with 100 mb.
        3. ```t```, ```c``` - Set boot partition type to W95 FAT32 (LBA)
        4. ```n```, ```p```, ```2```, ```return```, ```return``` - Create system partition in the remaining space.
        5. ```w``` - Write partition layout to SD Card and quit.

    3. Format the newly created partitions.
    ```
    # mkfs.vfat /dev/sdX1
    # mkfs.ext4 /dev/sdX2
    ```

    4. Mount directories into file system.
    ```
    # cd /mnt
    # mkdir boot
    # mkdir root
    # mount /dev/sdX1 /mnt/boot
    # mount /dev/sdX2 /mnt/root
    ```

    5. Download and extract ArchLinux Raspberry Pi 4 image.
    ```
    # wget http://os.archlinuxarm.org/os/ArchLinuxARM-rpi-4-latest.tar.gz
    # tar xpf ArchLinuxARM-rpi-4-latest.tar.gz -C /mnt/root
    # sync
    ```

    6. Prepare boot partition and unmount partitions
    ```
    # mv /mnt/root/boot/* /mnt/boot
    # umount /mnt/root /mnt/boot
    ```

2. **Configure ArchLinux Image**

    1. Put the prepared SD Card into the Raspberry Pi 4, connect a keyboard, power supply, ethernet cable and a monitor and boot up.

    2. Login as root: (password: root, user: alarm, password: alarm)

    3. Initialize the pacman keyring
    ```
    # pacman-key --init
    # pacman-key --populate archlinuxarm
    ```
    4. Activate i2c, spi and disable uart. Edit ```/boot/config.txt``` with ```nano``` and ajust the entries.
    ```
    dtparam=i2c_arm=on
    dtparam=spi=on
    dtoverlay=pi3-miniuart-bt-overlay
    enable_uart=0
    max_framebuffers=2
    ```

    5. Disable audit messages. Append ```audit=0 quiet loglevel=3``` to the line in ```/boot/cmdline.txt```. Additionally, run
    ```
    # systemctl mask systemd-journald-audit.socket
    ```

    6. Create ```/etc/udev/rules.d/larry.rules``` and add the access rules for i2c. Add the following lines
    ```
    SUBSYSTEM=="input", GROUP="input", MODE="0660"
    SUBSYSTEM=="i2c-dev", GROUP="i2c", MODE="0660"
    SUBSYSTEM=="spidev", GROUP="spi", MODE="0660"
    SUBSYSTEM=="bcm2835-gpiomem", GROUP="gpio", MODE="0660"
    ```

    7. Add required system groups and add the user ```alarm``` to the necessary system groups. 
    ```
    # groupadd spi
    # groupadd i2c
    # groupadd gpio
    # usermod -a -G gpio,spi,i2c,video alarm
    ```

    8. Update the system.
    ```
    # pacman -Syu
    ```

    9. Install basic development tools.
    ```
    # pacman -S base-devel cmake gcc git lua glm glew mesa v4l-utils wiringpi screen inetutils sudo 
    ```
    When you should enter a selection always use the default value.

    10. Configure sudo for passwordless root access from alarm user. Create file ```/etc/sudoers.d/10-wheel-no-passwd``` and add the following line.
    ```
    %wheel ALL=(ALL) NOPASSWD: ALL
    ```

    11. (Optional) allow SSH-access by enabling the sshd
    ```
    # systemctl enable sshd
    ```

    12. Reboot so that the updated kernel and boot option have a chance to get active.
    ```
    # reboot
    ```

3. **Install Access Point**

    1. Install required software
    ```
    # pacman -S hostapd dnsmasq
    ```

    2. Activate wlan by configuring hostapd. Edit the file ```/etc/hostapd/hostapd.conf``` and look here for the ```ssid=``` entry. Change it to ```ssid=larry```

    3. Setup IP settings for the access point device. Create the file ```/etc/systemd/network/10-hostapd.network``` and add the following lines
    ```
    [Match]
    Name=wlan0
    [Network]
    Address=10.0.0.1/8
    ```

    4. Configure the DHCP service. Edit the file ```/etc/dnsmasq.conf``` and add the following lines at the end of the file.
    ```
    interface=wlan0
    dhcp-range=10.0.0.3,10.0.0.20,12h
    no-resolv
    address=/#/10.0.0.1
    ```

    5. Adjust systemd DNS. Edit the file ```/etc/systemd/resolved.conf```, uncomment the following entry and set it to ```no```.
    ```
    DNSStubListener=no
    ```

    6. Activate HostAPD only after wlan0 is loaded. Edit the file ```/usr/lib/systemd/system/hostapd.service``` and add the following lines in the ```Unit``` section after deleting the line ```After=network.target```.
    ```
    BindsTo=sys-subsystem-net-devices-wlan0.device
    After=sys-subsystem-net-devices-wlan0.device
    ```

    7. Enable Access Point services
    ```
    # systemctl enable dnsmasq
    # systemctl enable hostapd
    ```

    8. Reboot, the wlan ```larry``` should come up on its own and you should be able to connect to it.
    ```
    # reboot
    ```

4. **Install librealsense2 for Stereo and Tracking Camera Support**

    1. Clone repository
    ```
    # git clone https://github.com/IntelRealSense/librealsense
    ```

    2. Setup device permissions.
    ```
    # cd librealsense
    # sudo ./scripts/setup_udev_rules.sh
    ```

    3. Correct wrong compile flags in librealsense. Open file
     ```CMake/unix_config.cmake``` and update lines to the following
      extract.
    ```
    # if(${MACHINE} MATCHES "arm-linux-gnueabihf")
    #     set(CMAKE_C_FLAGS   "${CMAKE_C_FLAGS}   -mfpu=neon -mfloat-abi=hard -ftree-vectorize -latomic")
    #     set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -mfpu=neon -mfloat-abi=hard -ftree-vectorize -latomic")
    # elseif(${MACHINE} MATCHES "aarch64-linux-gnu")

        set(CMAKE_C_FLAGS   "${CMAKE_C_FLAGS}   -march=native -O2 -ftree-vectorize")
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -march=native -O2 -ftree-vectorize")

    # else()
    #     set(CMAKE_C_FLAGS   "${CMAKE_C_FLAGS}   -mssse3")
    #     set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -mssse3")
    #     set(LRS_TRY_USE_AVX true)
    # endif(${MACHINE} MATCHES "arm-linux-gnueabihf")
    ```
    Make sure that only the two lines after ```elseif(${MACHINE} MATCHES "aarch64-linux-gnu")```
    are uncommented as well as ```-march=native -O2``` is added and ```-mstrict-align``` is removed from both lines.

    3. Compile and install librealsense. This will take a while so grab a coffee!
    ```
    # cmake -Bbuild -H. -DBUILD_EXAMPLES=false -DCMAKE_BUILD_TYPE=Release -DFORCE_RSUSB_BACKEND=true
    # cd build
    # make -j4
    # sudo make install
    # sudo ln -s /usr/local/lib/lib* /usr/lib/
    ```

5. **ArchLinux First Steps - Overview of Important Commands**
    
    Here is an overview of the most important commands in your new 
    ArchLinux Raspberry Pi distribution. For further questions I suggest the excellent ArchLinux Wiki (https://wiki.archlinux.org)

    * Update your system
    ```
    # pacman -Syu
    ```

    * Search for a package
    ```
    # pacman -Ss PACKAGE_NAME
    ```

    * Install package
    ```
    # pacman -S PACKAGE_NAME
    ```

    * Remove package
    ```
    # pacman -R PACKAGE_NAME
    ```


## Install larry-services on Larry
Log into Larry as ```alarm``` user (password: alarm) and perform the following steps.

1. Clone larry-services repository
```
# git clone https://gitlab.com/el.chris/larry-services.git
```

2. Build larry-services
```
# cd larry-services
# cmake -Bbuild -H. -DCMAKE_BUILD_TYPE=Release -DENABLE_GPIO_SUPPORT=ON
# cd build
# make -j4
```

3. If you would like to start a single service you always have to start the RouDi daemon first and then the service itself.  Change into the root 
folder of larry-services and type the following. (example start 
```Camera``` service)
```
# ./build/RouDi &
# ./build/services/Camera
```

4. If you would like to start all services at once type
```
# ./startAllServices.sh
```
If you would like to stop this service monitor press ```CTRL+a``` and then ```\```. A popup line appears and asks if you really want to terminate the session - press ```y```. 

(*Hint: If no message appears your terminal is too small, resize your terminal window and try again.*)

## Build Larry-UI and connect to Larry
Now that larry is up and running with all the services we need to connect to
larry to remotely control him. For this task we use Larry-UI which can be build
with the following steps on your PC/Laptop not on Larry!

1. Install required software packages

Larry UI uses the 3Delch engine which requires the following packages to be installed.
 * lua >= 5.2
 * assimp
 * cmake
 * glew
 * glm
 * sdl2
 * sdl2_image
 * sdl2_ttf
 * opencv (for object recognition service, maybe opencv needs also qt5)
 * googletest
 * googlemock

Additionally, Larry UI requires cmake >= 3.14 to be installed.

2. Clone larry-ui repository
```
# git clone https://gitlab.com/el.chris/larry-ui.git
```

3. Get 3rd party dependencies
```
# cd larry-ui
# git clone https://gitlab.com/el.chris/3delch-3rd-party.git
# mv 3delch-3rd-party data
```

4. Build larry-ui
```
# cmake -Bbuild -H. -DCMAKE_BUILD_TYPE=Release
# cd build
# make -j4 larry-ui
```

5. Connect to ```larry``` WLan

Larry has its own access point so that you can drive around without
having any cables connected. Therefore, you have to connect to the
open ```larry``` wlan.

6. Open another terminal and connect to larry via SSH and start the larry-services.

Larry's IP-Address is 10.0.0.1 and the DNS-server is configured in a
way that every DNS requests points to 10.0.0.1.
```
# ssh alarm@10.0.0.1        (password: alarm)
# cd larry-services
# ./startAllServices
```

7. Start the Larry-UI on your PC
In order to run larry-ui you have to leave the build directory
```
# cd ..
# ./build/larry-ui
```

8. Using Larry-UI
Firstly, you have to enter the IP-Address 10.0.0.1 and connect to larry. After that you can drive around with the ```wasd``` keys and
move the camera with ```q```, ```e```, ```r``` and ```f```. Additionally you can turn on and off some colored LEDs with ```z```, ```x```, ```c``` and ```v```.

If you would like to use the Larry-UI without Larry for experimental purposes you
can take a look at the [Larry-UI Readme](https://gitlab.com/el.chris/larry-ui/blob/master/README.md)