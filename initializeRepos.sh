#!/bin/bash

REPOS=(el.chris/3delch.git \
       el.chris/larry-services.git \
       el.chris/larry-ui.git \
       el.chris/iceoryx_on_fire.git
   )


ssh_clone() {
    echo -e "  \e[92mCloning (SSH):\e[0m $1"
    git clone git@gitlab.com:$1
    echo
}

https_clone() {
    echo -e "  \e[92mCloning (HTTPS):\e[0m $1"
    git clone https://gitlab.com/$1
    echo
}

update() {
    echo -e "  \e[96mUpdating:\e[0m $1"
    cd $2
    git pull
    cd ..
    echo
}

remove() {
    echo -e "  \e[91mRemoving:\e[0m $1"
    rm -rf $2
    echo
}

perform() {
    dir=$(echo $1 | sed -s "s/.*\/\(.*\).git/\1/")
    
    if [ $DO_REMOVE -eq 1 ]; then
        remove $1 $dir
    elif [ $DO_REMOVE -eq 0 ]; then
        if [ -d $dir ]; then
            update $1 $dir
        else
            if [ $USE_SSH -eq 0 ]; then
                https_clone $1
            elif [ $USE_SSH -eq 1 ]; then
                ssh_clone $1
            fi
        fi
    fi
}

help() {
    echo ""
    echo "  $0 [https,ssh,remove] (OPTIONAL: REPO_ID)"
    echo ""
    echo "    options:"
    echo "      https        - clone repos via https"
    echo "      ssh          - clone repos via ssh"
    echo "      remove       - remove all cloned repos"
    echo ""
    echo "    managed repos"
counter=1
for i in "${REPOS[@]}"
do
    echo "      $counter. $i"
    let counter++
done
    echo ""
    exit
}

USE_SSH=2
DO_REMOVE=2

if [[ $1 == "ssh" ]]; then
    USE_SSH=1
    DO_REMOVE=0
elif [[ $1 == "https" ]]; then
    USE_SSH=0
    DO_REMOVE=0
elif [[ $1 == "remove" ]]; then
    DO_REMOVE=1
else
    help
fi

if [[ -z $2 ]]; then
    for i in "${REPOS[@]}"
    do
        perform $i
    done
else
    let repoID=$2-1
    perform ${REPOS[$repoID]}
fi
